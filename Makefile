up:
	docker-compose up -d
	docker exec -it blogger-nodejs sh -c 'exec npm install'
	docker exec -it blogger-nodejs sh -c 'exec npm start'

down:
	docker-compose down

in:
	docker exec -it blogger-nodejs /bin/sh

run:
	docker exec -it blogger-nodejs sh -c 'exec npm install'
	docker exec -it blogger-nodejs sh -c 'exec npm start'

rebuild:
	docker-compose up -d --build