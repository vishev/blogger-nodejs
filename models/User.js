const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const Schema = mongoose.Schema;
let uniqueValidator = require('mongoose-unique-validator')

const UserSchema = new Schema({
    username: {
        type: String,
        required: [true, "Please provide username"],
        unique: true
    },
    password: {
        type: String,
        required: [true, "Please provide password"]
    },
    email: {
        type: String,
        required: [true, "Please provide an email"],
        unique: true
    },
    firstname: String,
    lastname: String,
    avatar: String
});

UserSchema.plugin(uniqueValidator)

// we tell Monoggose that before we save any rcord into the Users chema ot Users collection, execute the function passed into 2nd argument. 
// This lets us changes user data before saving into the database.
UserSchema.pre('save', function(next) {
    const user = this
    bcrypt.hash(user.password, 10, (error, hash) => {
        user.password = hash
        next()
    })
})

const User = mongoose.model('User', UserSchema);
module.exports = User