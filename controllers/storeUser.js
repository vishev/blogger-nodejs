const User = require('../models/User.js')
const path = require('path')

module.exports = (req, res) => {
    if (req.files === null) {
        const validationErrors = new Array("Avatar is missing");
        req.flash('validationErrors',validationErrors)
        req.flash('data', req.body)

        return res.redirect('/auth/register')
    } else {
        const avatar = req.files.avatar
        avatar.mv(path.resolve(__dirname, '..', 'public/blog_media', avatar.name), async (error) => {
            await User.create({
                ...req.body,
                avatar: '/blog_media/' + avatar.name
            }).catch(err => { 
                const validationErrors = Object.keys(err.errors).map(key => err.errors[key].message)
                req.flash('validationErrors',validationErrors)
                req.flash('data', req.body)

                return res.redirect('/auth/register')
            });
            
            res.redirect('/')
        })
    }
}