module.exports = (req, res) => {
    let username = ""
    let password = ""
    let email = ""
    let firstname = ""
    let lastname = ""

    const data = req.flash('data')[0]

    if (typeof data != 'undefined') {
        username = data.username
        password = data.password
        email = data.email
        firstname = data.firstname
        lastname = data.lastname
    }

    res.render('register', {
        errors: req.flash('validationErrors'),
        username: username,
        password: password,
        email: email,
        firstname: firstname,
        lastname: lastname
    })
}