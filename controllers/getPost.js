const BlogPost = require('../models/BlogPost.js')

module.exports = async (req,res) => {
    const blogPostResult = await BlogPost.findById(req.params.id)
    res.render('post', {
        blogpost:blogPostResult
    })
}