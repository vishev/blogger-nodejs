/**
 * Middleware
 */
const express = require('express')
const app = express()
const ejs = require('ejs');
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const expressSession = require('express-session')
const flash = require('connect-flash')


const customMiddleware = require('./middleware/customMiddleware')
const validateMiddleware = require('./middleware/validateMiddleware')
const authMiddleware = require('./middleware/authMiddleware')
const redirectIfAuthenticated = require('./middleware/redirectIfAuthenticatedMiddleware')

/**
 * Controllers
 */
const newPostController = require('./controllers/newPost')
const homeController = require('./controllers/home')
const getPostController = require('./controllers/getPost')
const storePostController = require('./controllers/storePost')
const newUserController = require('./controllers/newUser')
const storeUserController = require('./controllers/storeUser')
const loginController = require('./controllers/login')
const loginUserController = require('./controllers/loginUser')
const logoutController = require('./controllers/logout')

mongoose.connect('mongodb://blogger-nodejs-mongodb/my_database', {useNewUrlParser: true})

/**
 * Setup view engine and views folder
 */
app.set('view engine', 'ejs')

/**
 * Setup middleware(s)
 */
app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(fileUpload())
app.use(expressSession({
    secret: 'keyboard cat'
}))
app.use(flash())

// Just for fun , custom middlewares
app.use(customMiddleware)
app.use('/posts/store', validateMiddleware)

app.listen(3000, () => {
    console.log('App listening on port 3000')
})

// This is how to prepare a global variable in NodeJS !!!
global.loggedIn = null;
app.use('*', (req, res, next) => {
    loggedIn = req.session.userId;
    next()
})

app.get('/', homeController)
app.get('/post/:id', getPostController)
app.get('/posts/new', authMiddleware, newPostController)
app.post('/posts/store', authMiddleware, storePostController)
app.get('/auth/register', redirectIfAuthenticated,newUserController)
app.post('/users/register', redirectIfAuthenticated,storeUserController)
app.get('/auth/login', redirectIfAuthenticated,loginController)
app.post('/users/login', redirectIfAuthenticated,loginUserController)
app.get('/auth/logout', logoutController)

// Prepare Not Found page
app.use((req, res) => {
    res.render('notfound')
})