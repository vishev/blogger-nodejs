module.exports = (req, res, next) => {
    console.log('Custom middleware called')

    // tells Express that the middleware is done and Express should call the next middleware function
    // if you remove next() the app will hang as you have not told Express to proceed, all middlewares called by app.use calls next()
    next()
}